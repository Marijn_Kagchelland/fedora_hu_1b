#define MINDISTANCE 40
#define SetDistance 80

//Global Timer Compare Register Array's   |  comments achter de Array's zijn de uptime in microseconden
unsigned int Stop[2] = {28536,28536}; // 1500, 1500
unsigned int Links[2] = {28536, 28036}; //1500,2000
unsigned int Rechts[2] = {29036, 28536}; //1000, 1500
unsigned int Vooruit[2] = {29036,28036}; //1000, 2000
unsigned int Achteruit[2] = {28036, 29036}; //2000,1000
unsigned int Linksom[2] = {28036,28036}; //2000,2000
unsigned int Rechtsom[2] = {29036,29036}; //1000,1000

unsigned int Distance_mm , Distance_mm_old,i=0;


//____________________________________
//Sonar Functies
//____________________________________
void Sonar()
{
  PORTD |= 0x10; //PORT 4 HIGH
  us_Delay(10); //delay of 10µs
  PORTD &= ~(0x10); //PORT 4 LOW
  unsigned int Echo_pulse_duration = pulseIn(3,HIGH); //tijd in µs
  Distance_mm  = (Echo_pulse_duration * 0.343)/2;//berekenen afstand (µs * (mm/µs) 
}

 
//____________________________________
// Servo Functies
//____________________________________
ISR(TIMER1_COMPA_vect) //PORT 10
{
  PORTB &= ~(0b00000100);
}

ISR(TIMER1_COMPB_vect) //PORT 11
{
  PORTB &= ~(0b00001000);
}

ISR(TIMER1_OVF_vect)
{
  PORTB |= 0b00001100;
  TCNT1 = 25536;
}

void Tempo(unsigned int LRarray[2])
{
  if ((LRarray != Stop) || (LRarray != Rechtsom) || (LRarray != Linksom))
  {
     i++;
    if (i == 2)  
    {
      Sonar();
      i=0;
    }
    if (Distance_mm < MINDISTANCE) 
    {
      LRarray = Achteruit;
    }
    
    else
    {
      byte fout = (Distance_mm - SetDistance);
      long Update =(0.015*(fout*fout*fout));
      if (LRarray == Vooruit)
      {  
          if ((Vooruit[0] + Update) >= 29536)
          {
            LRarray[1] = 27536;
            LRarray[0] = 29536;
          }
          else if ((Vooruit[0] + Update) <= 28536)
          {
            LRarray[1] = 28536;
            LRarray[0] = 28536;
          }
          else
          {
            LRarray[0] += Update;
            LRarray[1] -= Update;
            Vooruit[0] += Update;
            Vooruit[1] -= Update;
          }
        
        /*else if (LRarray == Links)
        {
          
        }
        else if (LRarray == Rechts)
        {
          
        }*/
      }
    }
    //delay(1000);
  }
  OCR1B = LRarray[1]; //port 11
  OCR1A = LRarray[0]; //port 10 
}


//____________________________________
//Dealay functie's
//____________________________________
void ms_Delay(unsigned int ms)
{
  for (unsigned i; i <ms; i++)
  {
    TCNT2 = 0;
    while(TCNT2 < 63);
  }
}

void us_Delay(unsigned long tiks)
{
  for (unsigned long i; i <tiks; i++)
  {
    TCNT2 = 0;
    while(TCNT2 < 16);
  }
}

 
//____________________________________
//Main & Settings
//____________________________________
void setup() 
{
  //sonar
  DDRD |= 0x10;//PORT 4 OUTPUT Bruin
  DDRD &= ~(0x08);// PORT 3 INPUT Rood
   //servo
  DDRB |= (0b00001100);
  TCCR1A = 0; 
  TCCR1B = 0b00000010;
  TCCR1C = 0;
  TCNT1 = 25536; //start telling
  OCR1A = 29536; //port10
  OCR1B = 29536; //port11
  TIMSK1 = 0b00000111;
  
  //µs Delay timer
  TCCR0A = 0;
  TCCR0B = 0b00000100;
  TIMSK0 = 0;
  
  //ms Timer
  TCCR2A = 0b00000000;
  TCCR2B = 0b00000001;
  TIMSK2 = 0b00000000;
  sei();
}

void loop() 
{   
  Tempo(Vooruit);
  ms_Delay(60);
}


//____________________________________
//END
//____________________________________
